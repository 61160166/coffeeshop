/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeprogect.poc.TestSelectEmployee;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author Nopparuth
 */
public class EmployeeDao implements DaoInterface<Employee > {

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Employee (FirstName, LastName, Tel, ID_Card, Type, UserName, Password) VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getFirstName());
            stmt.setString(2,object.getLastName());
            stmt.setString(3,object.getTel());
            stmt.setString(4,object.getIdCard());
            stmt.setString(5,object.getType());
            stmt.setString(6,object.getUserName());
            stmt.setString(7,object.getPassword());
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
            ResultSet result = stmt.getGeneratedKeys();
            
            while(result.next()){
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Employee_ID, FirstName, LastName, Tel, ID_Card, Type, UserName, Password FROM Employee";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
               int id = result.getInt("Employee_ID");
               String FirstName = result.getString("FirstName");
               String LastName = result.getString("LastName");
               String Tel = result.getString("Tel");
               String IDCard = result.getString("ID_Card");
               String Type = result.getString("Type");
               String UserName = result.getString("UserName");
               String Password = result.getString("Password");
               Employee employee = new Employee(id, FirstName, LastName, Tel, IDCard, Type, UserName, Password);
               list.add(employee);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Employee_ID, FirstName, LastName, Tel, ID_Card, Type, UserName, Password FROM Employee WHERE Employee_ID=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()){
               int epid = result.getInt("Employee_ID");
               String FirstName = result.getString("FirstName");
               String LastName = result.getString("LastName");
               String Tel = result.getString("Tel");
               String IDCard = result.getString("ID_Card");
               String Type = result.getString("Type");
               String UserName = result.getString("UserName");
               String Password = result.getString("Password");
               Employee employee = new Employee(epid, FirstName, LastName, Tel, IDCard, Type, UserName, Password);
               return employee;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Employee WHERE Employee_ID = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Employee SET FirstName = ?, LastName = ?, Tel = ?, ID_Card = ?, Type = ?, UserName = ?,Password = ?  WHERE Employee_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getFirstName());
            stmt.setString(2,object.getLastName());
            stmt.setString(3,object.getTel());
            stmt.setString(4,object.getIdCard());
            stmt.setString(5, object.getType());
            stmt.setString(6, object.getUserName());
            stmt.setString(7, object.getPassword());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
    public static void main(String[] args) {
        EmployeeDao dao = new EmployeeDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Employee(-1, "Davikha", "Hone", "0988888888", "1200000111111", "Full Time", "Davikha","password"));
        System.out.println("id: "+ id);
        Employee lastEmployee = dao.get(id);
        System.out.println("last employee: "+lastEmployee);
        lastEmployee.setUserName("username");
        int row = dao.update(lastEmployee);
        Employee updateEmployee = dao.get(id);
        System.out.println("update employee: " + updateEmployee);
        dao.delete(id);
        Employee deleteEmployee = dao.get(id);
        System.out.println("delete employee: " + deleteEmployee);
    } 
    
}
