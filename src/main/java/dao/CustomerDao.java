/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeprogect.poc.TestSelectCustomer;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author Dell
 */
public class CustomerDao implements DaoInterface<Customer>{
    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Customer (Tel,LastName,FirstName) VALUES ( ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Customer customer = new Customer(-1, "0894355566", "Thana", "Wararat");
            stmt.setString(1,customer.getTel());
            stmt.setString(2,customer.getLastName());
            stmt.setString(3, customer.getFirstName());
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
            ResultSet result = stmt.getGeneratedKeys();
            while(result.next()){
                id = result.getInt(1);
            }
            System.out.println("Affect row " + row + " id: " + id);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Customer_ID, Tel, LastName, FirstName FROM Customer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
               int id = result.getInt("Customer_ID");
               String Tel = result.getString("Tel");
               String LastName = result.getString("LastName");
               String FirstName = result.getString("FirstName");
               Customer customer = new Customer(id, Tel, LastName, FirstName);
               list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Customer_ID, Tel, LastName, FirstName FROM Customer WHERE Customer_ID";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()){
               int cid = result.getInt("Customer_ID");
               String Tel = result.getString("Tel");
               String LastName = result.getString("LastName");
               String FirstName = result.getString("FirstName");
               Customer customer = new Customer(cid, Tel, LastName, FirstName);
               return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Customer WHERE Customer_ID = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Customer SET Tel = ? ,LastName = ?,FirstName = ? WHERE Customer_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,object.getTel());
            stmt.setString(2,object.getLastName());
            stmt.setString(3, object.getFirstName());
            stmt.setInt(4,object.getId());
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
    public static void main(String[] args) {
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Customer(-1, "0989997555", "Thai", "Thana"));
        System.out.println("id: "+ id);
        Customer lastCustomer = dao.get(id);
        System.out.println("last customer: " + lastCustomer);
        lastCustomer.setFristName("FirstName");
        int row = dao.update(lastCustomer);
        Customer updateCustomer = dao.get(id);
        System.out.println("update customer: " + updateCustomer);
        dao.delete(id);
        Customer deleteCustomer = dao.get(id);
        System.out.println("delete customer: " + deleteCustomer);
    } 
    
}
