/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeprogect.poc.TestSelectMember;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Member;
import model.Product;

/**
 *
 * @author Dell
 */
public class MemberDao implements DaoInterface<Member>{

    @Override
    public int add(Member object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Member (Tel,Name) VALUES (?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Member member = new Member(-1, "089777699", "playud jungama"); 
            stmt.setString(1,member.getTel());
            stmt.setString(2,member.getName());
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
            ResultSet result = stmt.getGeneratedKeys();
            while(result.next()){
                id = result.getInt(1);
            }
            System.out.println("Affect row " + row + " id: " + id);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectMember.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Member> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Member_ID, Tel, Name FROM Member";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
               int id = result.getInt("Member_ID"); 
               String Tel = result.getString("Tel");
               String Name = result.getString("Name");          
               Member member = new Member(id, Tel, Name);
               list.add(member);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectMember.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Member get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Member_ID, Tel, Name FROM Member";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
               int mid = result.getInt("Member_ID"); 
               String Tel = result.getString("Tel");
               String Name = result.getString("Name");          
               Member member = new Member(mid, Tel, Name);
               return member;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectMember.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Member WHERE Member_ID = ? ";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,6);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectMember.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Member object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Member SET Tel = ? ,Name = ? WHERE Member_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,"Steve Loland");
            stmt.setString(2,"0999999888");
            stmt.setInt(3,1);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectMember.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
    
    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Product(-1, "KafaeYen", 20, 20.0, "coffee"));
        System.out.println("id: "+ id);
        Product lastProduct = dao.get(id);
        System.out.println("last product: "+lastProduct);
        lastProduct.setPrice(100);
        int row = dao.update(lastProduct);
        Product updateProduct = dao.get(id);
        System.out.println("update product: " + updateProduct);
        dao.delete(id);
        Product deleteProduct = dao.get(id);
        System.out.println("delete product: " + deleteProduct);
    }
    
    
}
