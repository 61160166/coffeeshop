/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

/**
 *
 * @author Dell
 */
public class NewMain extends javax.swing.JDialog{

    /**
     * @param args the command line arguments
     */
    
    public NewMain(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
//        initComponents();
        EmployeePanel panel = new EmployeePanel();
        this.add(panel);
        this.pack();
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Test dialog = new Test(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    
}
