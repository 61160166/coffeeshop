/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nopparuth
 */
public class EmpType {
    int id;
    String type;
    double moneyHours;

    public EmpType(int id, String type, double moneyHours) {
        this.id = id;
        this.type = type;
        this.moneyHours = moneyHours;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getMoneyHours() {
        return moneyHours;
    }

    public void setMoneyHours(double moneyHours) {
        this.moneyHours = moneyHours;
    }

    @Override
    public String toString() {
        return "EmpType{" + "id=" + id + ", type=" + type + ", moneyHours=" + moneyHours + '}';
    }
    
}
