/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nopparuth
 */
public class OrderDetail {
    int id;
    int amount;
    double price;
    int productID;
    int listProductID;
    int billID;

    public OrderDetail(int id, int amount, double price, int productID, int listProductID, int billID) {
        this.id = id;
        this.amount = amount;
        this.price = price;
        this.productID = productID;
        this.listProductID = listProductID;
        this.billID = billID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getListProductID() {
        return listProductID;
    }

    public void setListProductID(int listProductID) {
        this.listProductID = listProductID;
    }

    public int getBillID() {
        return billID;
    }

    public void setBillID(int billID) {
        this.billID = billID;
    }

    @Override
    public String toString() {
        return "OrderDetail{" + "id=" + id + ", amount=" + amount + ", price=" + price + ", productID=" + productID + ", listProductID=" + listProductID + ", billID=" + billID + '}';
    }
    
    
}
