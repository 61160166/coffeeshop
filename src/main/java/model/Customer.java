/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nopparuth
 */
public class Customer {
    private int id;
    private String tel;
    private String lastName;
    private String firstName;

    public Customer(int id, String tel, String lastName, String firstName) {
        this.id = id;
        this.tel = tel;
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", tel=" + tel + ", lastName=" + lastName + ", firstName=" + firstName + '}';
    }

    public void setFristName(String firstName) {
        this.firstName = firstName;
    }

}
