/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nopparuth
 */
public class Member {
    int id;
    String Tel;
    String Name;

    public Member(int id, String Tel, String Name) {
        this.id = id;
        this.Tel = Tel;
        this.Name = Name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String Tel) {
        this.Tel = Tel;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    @Override
    public String toString() {
        return "Member{" + "id=" + id + ", Tel=" + Tel + ", Name=" + Name + '}';
    }
    
}
