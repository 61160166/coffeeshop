/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author Nopparuth
 */
public class Bill {
    int id;
    Timestamp Date;
    int orderDetailID;
    int empID;
    int customerID;

    public Bill(int id, Timestamp Date, int orderDetailID, int empID, int customerID) {
        this.id = id;
        this.Date = Date;
        this.orderDetailID = orderDetailID;
        this.empID = empID;
        this.customerID = customerID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getDate() {
        return Date;
    }

    public void setDate(Timestamp Date) {
        this.Date = Date;
    }

    public int getOrderDetailID() {
        return orderDetailID;
    }

    public void setOrderDetailID(int orderDetailID) {
        this.orderDetailID = orderDetailID;
    }

    public int getEmpID() {
        return empID;
    }

    public void setEmpID(int empID) {
        this.empID = empID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", Date=" + Date + ", orderDetailID=" + orderDetailID + ", empID=" + empID + ", customerID=" + customerID + '}';
    }
    
    
}
