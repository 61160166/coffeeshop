/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nopparuth
 */
public class Product {
    private int id;
    private String name;
    private int Amount;
    private double price;
    private String Type;

    public Product(int id, String name, int Amount, double price, String Type) {
        this.id = id;
        this.name = name;
        this.Amount = Amount;
        this.price = price;
        this.Type = Type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int Amount) {
        this.Amount = Amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", Amount=" + Amount + ", price=" + price + ", Type=" + Type + '}';
    }
    
    
}
