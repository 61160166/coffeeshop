/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author Nopparuth
 */
public class ListProduct {
    int id;
    Timestamp DateTime;
    String memberTel;
    int empID;

    public ListProduct(int id, Timestamp DateTime, String memberTel, int empID) {
        this.id = id;
        this.DateTime = DateTime;
        this.memberTel = memberTel;
        this.empID = empID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getDateTime() {
        return DateTime;
    }

    public void setDateTime(Timestamp DateTime) {
        this.DateTime = DateTime;
    }

    public String getMemberTel() {
        return memberTel;
    }

    public void setMemberTel(String memberTel) {
        this.memberTel = memberTel;
    }

    public int getEmpID() {
        return empID;
    }

    public void setEmpID(int empID) {
        this.empID = empID;
    }

    @Override
    public String toString() {
        return "ListProduct{" + "id=" + id + ", DateTime=" + DateTime + ", memberTel=" + memberTel + ", empID=" + empID + '}';
    }
    
    
}
