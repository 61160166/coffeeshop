/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeprogect.poc;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nopparuth
 */
public class TestUpdateEmpType {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "UPDATE EmpType SET Type = ? ,Money_Hours = ?WHERE Emp_Type_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,"Part Time");
            stmt.setDouble(2,35.00);
            stmt.setInt(3,3);
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmpType.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       
        db.close();
    }
}
