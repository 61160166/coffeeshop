/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeprogect.poc;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nopparuth
 */
public class TestUpdateCustomer {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "UPDATE Customer SET Tel = ? ,LastName = ?,FirstName = ?  WHERE Customer_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,"0899993433");
            stmt.setString(2,"Siri");
            stmt.setString(3, "wilawan");
            stmt.setInt(4,1);
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
       
        db.close();
    }
}
