/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeprogect.poc;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Bill;

/**
 *
 * @author Nopparuth
 */
public class TestSelectBill {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Bill_ID, Date, Order_DeTail_ID, Employee_ID, Customer_ID FROM Bill";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
               int id = result.getInt("Bill_ID");
//               Timestamp Date = result.getTimestamp("Date");
               int oderDetailID = result.getInt("Order_DeTail_ID");
               int empID = result.getInt("Employee_ID");
               int customerID = result.getInt("Customer_ID");
               Bill bill = new Bill(id, null, oderDetailID, empID, customerID);
               System.out.println(bill);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectBill.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
