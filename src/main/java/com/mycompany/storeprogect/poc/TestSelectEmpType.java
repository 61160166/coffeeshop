/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeprogect.poc;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.EmpType;

/**
 *
 * @author Nopparuth
 */
public class TestSelectEmpType {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Emp_Type_ID, Type, Money_Hours FROM EmpType";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
               int id = result.getInt("Emp_Type_ID");
               String Type = result.getString("Type");
               double Money_Hours = result.getDouble("Money_Hours"); 
               EmpType emptype = new EmpType(id, Type, Money_Hours);
               System.out.println(emptype);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectEmpType.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
