/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeprogect.poc;

import database.Database;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Member;

/**
 *
 * @author Nopparuth
 */
public class TestSelectMember {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Member_ID, Tel, Name FROM Member";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){
               int id = result.getInt("Member_ID"); 
               String Tel = result.getString("Tel");
               String Name = result.getString("Name");          
               Member member = new Member(id, Tel, Name);
               System.out.println(member);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectMember.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
