/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeprogect.poc;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Member;

/**
 *
 * @author Nopparuth
 */
public class TestInsertMember {
     public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "INSERT INTO Member (Tel,Name) VALUES (?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Member member = new Member(-1, "089777699", "playud jungama"); 
            stmt.setString(1,member.getTel());
            stmt.setString(2,member.getName());
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
            ResultSet result = stmt.getGeneratedKeys();
            int id = -1;
            while(result.next()){
                id = result.getInt(1);
            }
            System.out.println("Affect row " + row + " id: " + id);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectMember.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    }
}
